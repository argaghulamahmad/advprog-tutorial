package sorting;

import java.util.Arrays;

public class Sorter {

    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */

    public static int[] quickSort(int[] inputArr) {
        Arrays.sort(inputArr);
        return inputArr;
    }

}
