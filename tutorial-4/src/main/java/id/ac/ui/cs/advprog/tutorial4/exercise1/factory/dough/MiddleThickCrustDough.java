package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class MiddleThickCrustDough implements Dough {
    public String toString() {
        return "Middle Thick Crust Dough";
    }
}
